require('dotenv/config');
const fritz = require('fritzapi');
const request = require('request');
const Prometheus = require('prom-client');
const app = require('express')();
const ON_DEATH = require('death');

// Create Prometheus Gauges (https://prometheus.io/docs/concepts/metric_types) to track the down- and upstream values
const downstreamGauge = new Prometheus.Gauge({
  name: 'fritzbox_downstream_bps',
  help: 'Downstream workload of the FritzBox in bits per second',
});
const upstreamGauge = new Prometheus.Gauge({
  name: 'fritzbox_upstream_bps',
  help: 'Upstream workload of the FritzBox in bits per second',
});

let sid,
  errCount = 0;

const getInetStats = async () => {
  if (!sid) {
    try {
      sid = await fritz.getSessionID(process.env.FRITZBOX_USER, process.env.FRITZBOX_PASSWORD, {
        url: process.env.FRITZBOX_URL,
      });
    } catch (err) {
      console.error(err);
      return;
    }
  }

  let url = process.env.FRITZBOX_URL + '/internet/inetstat_monitor.lua?1=1';
  url += `&sid=${sid}&myXhr=1&action=get_graphic&useajax=1&xhr=1`;
  const timestamp = +new Date();
  url += `&t${timestamp}=nocache`;

  request(url, (err, res, body) => {
    if (err || body.trim() === '') {
      console.error(err || 'Empty response');
      sid = undefined;
      if (errCount < 3) {
        errCount++;
        getInetStats();
      }
      return;
    }

    errCount = 0;
    const data = JSON.parse(body)[0];
    downstreamGauge.set(data.ds_bps_curr[0] * 8);
    upstreamGauge.set(data.us_realtime_bps_curr[0] * 8);
  });
};

const inetStatsInterval = setInterval(getInetStats, 5000);

const port = 9096;

// Answer requests from the Prometheus client
app.get('/metrics', (req, res) => {
  res.set('Content-Type', Prometheus.register.contentType);
  res.end(Prometheus.register.metrics());
});

const server = app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`);
});

// Clear the inet-stats interval and close the server on program exit
ON_DEATH((signal, err) => {
  clearInterval(inetStatsInterval);
  server.close();
});
