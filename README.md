## Prerequisites
- Install [Grafana](grafana.com) (default port 3000)
- Install [Prometheus](prometheus.io) (default port 9090)
- Create a .env file containing your FRITZBOX_URL, FRITZBOX_USER and FRITZBOX_PASSWORD

## Installing the npm packages
```
npm install
```

## Running the server
```
npm start
```

## Preparing Prometheus
- Add 'localhost:[Port]' to scrape_configs -> targets in prometheus/prometheus.yml

## Preparing Grafana
- Select Prometheus as data source (use the default port 9090)
- Create a new dashboard and select the desired metrics from Prometheus